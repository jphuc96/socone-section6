
// test the function

const request = require('supertest')
const app = require('../app')

describe('GET /api/hello', () => {
  it('responds 200 OK', (done) => {
    request(app)
      .get('/api/hello')
      .expect(200, done)
  })
})